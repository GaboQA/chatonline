<?php if ( ! defined( 'ABSPATH' ) ) exit;
?>
<div style="padding: 0 30px 30px 30px; border-bottom: 3px solid #ffffff;">
<div style="padding: 30px 0; font-size: 24px; text-align: center; line-height: 40px; color: #202124">¡Gracias por registrarte!<span style="display: block;">Haga clic en el siguiente enlace para activar su cuenta.</span></div>
<div style="padding: -1px 0 50px 0; text-align: center;"><a style="background: #222; color: #fff; padding: 12px 30px; text-decoration: none; border-radius: 3px; letter-spacing: 0.3px;" href="{account_activation_link}">Activar cuenta</a></div></div>
<div style="padding: 10px 0; font-size: 14px; text-align: center; line-height: 20px; color: #202124">Usuario: {email} o {username}</div>
