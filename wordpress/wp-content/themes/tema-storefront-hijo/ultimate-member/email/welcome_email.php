<div style="padding: 0 30px 30px 30px; border-bottom: 3px solid #eeeeee;">
<div style="padding: 30px 0; font-size: 24px; text-align: center; line-height: 40px;"><span style="">¡Gracias por registrarte!</span><span style="display: block;">Tu cuenta ya está activa.</span></div>
<div style="padding: 10px 0 50px 0; text-align: center;"><a style="background: #555555; color: #fff; padding: 12px 30px; text-decoration: none; border-radius: 3px; letter-spacing: 0.3px;" href="{login_url}">Inicie sesión en nuestro sitio</a></div>
<div style="padding: 20px;">Si tiene algún problema, póngase en contacto con nosotros <a style="color: #3ba1da; text-decoration: none;" href="mailto:{admin_email}">a admin_email</a></div>
</div>
<div style="color: #999; padding: 20px 30px;">
<div style="text-align: left;">¡Gracias!</div>
<div style="text-align: left;">El <a style="color: #3ba1da; text-decoration: none;" href="{site_url}">equipo de site_name</a></div>
</div>
</div>