(function ($) {
  var endpointAPI = "http://localhost:8000";
  var id_aut = 1;
  var nombre_usu2;
  var params = {
    id_aut: id_aut,
  };
  $.ajax(
    {
      type: "POST",
      url: endpointAPI + "/api/contacts",
      data: JSON.stringify(params),
      contentType: "application/json",
      success: function (res) {
        if (res.status_code == 201) {
          var arrayPartido = res.resource;
          for (var i = 0; i < arrayPartido.length; i++) {
            var mHTML3 = "<tr>";
            mHTML3 +=
              "<td>" +
              '<button  id="' +
              i +
              '" data-usuario="' +
              arrayPartido[i].id +
              '">' +
              arrayPartido[i].name +
              "</button>" +
              "</td>";
            mHTML3 += "</tr>";
            $("#idTabla").children("tbody").append(mHTML3);
            mHTML3 = "";
          }
          //setInterval(fun, 1000);
          $("td").click(function () {
            var row_index = $(this).parent();
            console.log(row_index.context.firstChild.dataset.usuario);
            console.log(row_index.context.firstChild.innerText);
            //Datos del usuario escogido
            user_conversacion = row_index.context.firstChild.innerText;
            id_conversacion = row_index.context.firstChild.dataset.usuario;
            localStorage.setItem("iduser", id_conversacion);
            localStorage.setItem("idaut", id_aut);
            localStorage.setItem("user_conversacion", user_conversacion);
            var params = {
              id_aut: id_aut,
              id_conversacion: id_conversacion,
            };
            $.ajax({
              type: "POST",
              url: endpointAPI + "/api/conversation/info",
              data: JSON.stringify(params),
              contentType: "application/json",
              success: function (res) {
                if (res.status_code == 201) {
                  console.log(res.resource);
                  mensajes = res.resource;
                  for (var i = 0; i < mensajes.length; i++) {
                    //console.log(mensajes[i].from);
                    //console.log(id_conversacion);
                    var to = mensajes[i].from;
                    if (to == id_conversacion) {
                      appendMessage(
                        user_conversacion,
                        null,
                        "left",
                        mensajes[i].text
                      );
                    }
                    if (to != id_conversacion) {
                      appendMessage(
                        "Administrador",
                        null,
                        "right",
                        mensajes[i].text
                      );
                    } else {
                      console.log("Mensaje sin autor");
                    }
                  }
                } else {
                  console.log(res.resource);
                }
              }.bind(this),
              error: function (xhr, status, err) {
                console.error(xhr, status, err.toString());
              }.bind(this),
            });
          });
        } else {
          console.log(res.resource);
        }
      }.bind(this),
      error: function (xhr, status, err) {
        console.error(xhr, status, err.toString());
      }.bind(this),
    },
    5000
  );
  function fun() {
    if (localStorage.getItem("iduser") != null) {
      var id_conversacion = localStorage.getItem("iduser");
      var id_aut = localStorage.getItem("idaut");
      var user_conversacion = localStorage.getItem("user_conversacion");
      var params = {
        id_aut: id_aut,
        id_conversacion: id_conversacion,
      };
      $.ajax(
        {
          type: "POST",
          url: endpointAPI + "/api/conversation/info",
          data: JSON.stringify(params),
          contentType: "application/json",
          success: function (res) {
            if (res.status_code == 201) {
              console.log(res.resource);
              mensajes = res.resource;
              for (var i = 0; i < mensajes.length; i++) {
                var to = mensajes[i].from;
                if (to == id_conversacion) {
                  appendMessage(
                    user_conversacion,
                    null,
                    "left",
                    mensajes[i].text
                  );
                }
                if (to != id_conversacion) {
                  appendMessage(
                    "Administrador",
                    null,
                    "right",
                    mensajes[i].text
                  );
                } else {
                  console.log("Mensaje sin autor");
                }
              }
            } else {
              console.log(res.resource);
            }
          }.bind(this),
          error: function (xhr, status, err) {
            console.error(xhr, status, err.toString());
          }.bind(this),
        },
        10
      );
    }
  }
})(jQuery);
