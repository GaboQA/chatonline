(function($){

    if( $('body.product-template-default').length){
        SendCustomerProductInterest();       
     }

     function SendCustomerProductInterest () {

        var productDiv = document.getElementsByClassName("type-product");
        var idDiv = $(productDiv).attr('id');
        var productId = idDiv.split('-');
        productId = productId[1];
        var endpointAPI = scriptParams.endpointAPI;
        var currentUserId = scriptParams.currentUserId;
        var params = {
            currentUserId: currentUserId,
            productId: productId
        };
        $.ajax({ 
            type:"POST",
            url:endpointAPI + '/api/CustomerDashboard/ProductInterested',
            data: JSON.stringify(params), 
            contentType: 'application/json',
            success: function(res) {
                if(res.status_code == 201){
                    //console.log(res.resource);
                }else{
                    //console.log(res.resource);
                }
            }.bind(this),
            error: function(xhr, status, err) {
                //console.error(xhr, status, err.toString());
            }.bind(this)
        });
    };
    /*
    function axios_send(endpointAPI, currentUserId, productId){
        console.log('End point API#: ' + endpointAPI);
        console.log('Current user ID#: ' + currentUserId);
        console.log('Product ID#: ' + productId);
        var config = {
            headers: { 
                'Content-Type': 'application/json;charset=UTF-8;application/x-www-form-urlencoded;',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Headers': 'Origin, X-Requested-With, Content-Type, Accept'
            }
        };
        var data = {
            currentUserId: currentUserId,
            productId: productId
        };
        axios.post(endpointAPI + '/api/CustomerDashboard/ProductInterested', data, config)
            .then((res) => {
                if(res.data.status_code == 201){
                    console.log(res.data);
                }
                else{
                    console.log('Respuesta con error');
                    console.log(res.data);
                }
            })
            .catch((error) => {
                console.log(error);
            });
    };*/

     /*  
    $(document).on("click",".type-product", function () {
        var liClicked = $(this).attr('id');
        var productId = liClicked.split('-');
        productId = productId[1];
        console.log(productId);
        //console.log($(this));
        //console.log('LI seleccionado#: ' + liClicked);
        //console.log('Id producto#: ' + productId[1]);
        var endpointAPI = scriptParams.endpointAPI;
        //console.log('End point API#: ' + endpointAPI);
        var currentUserId = scriptParams.currentUserId;
        //console.log('Current user ID#: ' + currentUserId);
        /*
        axios.get(
            endpointAPI + '/api/CustomerDashboard/ProductInterested', {
                headers: {'Accept': 'application/json'}
            })
            .then(response => {
                this.test = response.data;
                console.log(this.test);
            });*//*
        var config = {
            headers: { 
                'Content-Type': 'application/json;charset=UTF-8;application/x-www-form-urlencoded;',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Headers': 'Origin, X-Requested-With, Content-Type, Accept'
            }
        };
        var data = {
            currentUserId: currentUserId,
            productId: productId
        };
        axios.post(endpointAPI + '/api/CustomerDashboard/ProductInterested', data, config)
            .then((res) => {
                if(res.data.status_code == 201){
                    console.log(res.data);
                }
                else{
                    console.log('Respuesta con error');
                    console.log(res.data);
                }
            })
            .catch((error) => {
                console.log(error);
            });*/
    //});
})(jQuery);
