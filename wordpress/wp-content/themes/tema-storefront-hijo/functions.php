<?php
// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) exit;

// BEGIN ENQUEUE PARENT ACTION
// AUTO GENERATED - Do not modify or remove comment markers above or below:

if ( !function_exists( 'chld_thm_cfg_locale_css' ) ):
    function chld_thm_cfg_locale_css( $uri ){
        if ( empty( $uri ) && is_rtl() && file_exists( get_template_directory() . '/rtl.css' ) )
            $uri = get_template_directory_uri() . '/rtl.css';
        return $uri;
    }
endif;
add_filter( 'locale_stylesheet_uri', 'chld_thm_cfg_locale_css' );

if ( !function_exists( 'chld_thm_cfg_parent_css' ) ):
    function chld_thm_cfg_parent_css() {
        wp_enqueue_style( 'chld_thm_cfg_parent', trailingslashit( get_template_directory_uri() ) . 'style.css', array( 'storefront-gutenberg-blocks' ) );
    }
endif;

add_action( 'wp_enqueue_scripts', 'chld_thm_cfg_parent_css', 10 );

/*Styles*/
function estilos_login(){
    wp_enqueue_style('login-personal-css', site_url(). '/wp-content/themes/tema-storefront-hijo/css/login.css', array(), '5.5');
}
add_action('login_head', 'estilos_login');



//Funcion para devolverse al inicio de la pagina 
function url_inicio_login(){
    return site_url();
}
add_filter('login_headerurl','url_inicio_login');
function paymentpaypal_js(){
    wp_register_script('paymentpaypal_js', get_stylesheet_directory_uri(). '/js/paymentpaypal.js', array('jquery'), '1', true );
    wp_enqueue_script('paymentpaypal_js');
}
add_action("wp_enqueue_scripts", "paymentpaypal_js");
function wp_admin_order_details_style(){
    wp_register_style( 'wp_admin_order_details_style', site_url(). '/wp-content/themes/tema-storefront-hijo/css/wp_admin_order_details.css', array(), '5.5');
    wp_enqueue_style( 'wp_admin_order_details_style' );
}
add_action('admin_enqueue_scripts', 'wp_admin_order_details_style');

function titulo_login_url(){
    return "Volver al inicio";
}
add_filter('login_headertitle', 'titulo_login_url');

/*Javascript*/
function dashboard_js(){    
    if( is_user_logged_in() ){
        wp_register_script('dashboard_js', get_stylesheet_directory_uri(). '/js/dashboard.js', array('jquery'), '1', true );
        wp_enqueue_script('dashboard_js');
        $currentUserId = wp_get_current_user();
        $currentUserId = $currentUserId->ID;
        $endpointAPI = endpointAPI();
        $script_params = array(
            'currentUserId' => $currentUserId,
            'endpointAPI'   => $endpointAPI
        );
        wp_localize_script( 'dashboard_js', 'scriptParams', $script_params );
    }  
}
add_action("wp_enqueue_scripts", "dashboard_js");

function axios_js(){
    wp_register_script('axios_js', get_stylesheet_directory_uri(). '/js/axios.min.js', array('jquery'), '1', true );
    wp_enqueue_script('axios_js');
}
add_action("wp_enqueue_scripts", "axios_js");

function qs_js(){
    wp_register_script('qs_js', get_stylesheet_directory_uri(). '/js/qs.js', array('jquery'), '1', true );
    wp_enqueue_script('qs_js');
}
add_action("wp_enqueue_scripts", "qs_js");


function customer_verify_notification(){
    if( is_user_logged_in() ){

        global $post;
        $post_slug = $post->post_name;

        if($post_slug != "carrito" && $post_slug != "finalizar-compra"){//Cualquiera que no sea carrito o checkout

            if($post_slug != "notificaciones-cliente"){//Cualquiera que no sea carrito, checkout y tabla de notificaciones
                wp_register_script('customer_verify_notification', get_stylesheet_directory_uri(). '/js/customer_verify_notification.js', array('jquery'), '1', true );
                wp_enqueue_script('customer_verify_notification');
                $currentUserId = wp_get_current_user();
                $currentUserId = $currentUserId->ID;
                $endpointAPI = endpointAPI();
                $script_params = array(
                    'currentUserId' => $currentUserId,
                    'endpointAPI'   => $endpointAPI,
                );
                wp_localize_script( 'customer_verify_notification', 'scriptParams', $script_params );
            }else{//Ingresa/carga solo cuando estamos en la tabla de notificaciones, por si existen nuevos ingresar registros nuevos por jquery
                wp_register_script('customer_verify_notification_table', get_stylesheet_directory_uri(). '/js/customer_verify_notification_table.js', array('jquery'), '1', true );
                wp_enqueue_script('customer_verify_notification_table');
                $currentUserId = wp_get_current_user();
                $currentUserId = $currentUserId->ID;
                $endpointAPI = endpointAPI();
                $script_params = array(
                    'currentUserId' => $currentUserId,
                    'endpointAPI'   => $endpointAPI,
                );
                wp_localize_script( 'customer_verify_notification_table', 'scriptParams', $script_params );
            }

        }else{//Solo una vez, para notificaciones cuando se ingresa al carrito o checkout
            wp_register_script('customer_verify_notification_aux', get_stylesheet_directory_uri(). '/js/customer_verify_notification_aux.js', array('jquery'), '1', true );
            wp_enqueue_script('customer_verify_notification_aux');
            $currentUserId = wp_get_current_user();
            $currentUserId = $currentUserId->ID;
            $endpointAPI = endpointAPI();
            $script_params = array(
                'currentUserId' => $currentUserId,
                'endpointAPI'   => $endpointAPI,
            );
            wp_localize_script( 'customer_verify_notification_aux', 'scriptParams', $script_params );
        }
    }
}


/*Global*/
function endpointAPI() {
    global $endpointAPI;
    //**** Uncomment if you work in local - Work in local with port 8000 ****
    $endpointAPI = 'http://localhost:8000';
    //**** Uncomment if is ready to upload in AWS - Work in AWS with endpoint as follow ****
    //$endpointAPI = 'http://verdelimonapi-env.eba-xidsfjms.us-west-2.elasticbeanstalk.com';
    return $endpointAPI;
}
// END ENQUEUE PARENT ACTION
//Shordcode get pay


?>

