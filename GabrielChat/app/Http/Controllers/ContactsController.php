<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Message;
use App\Events\NewMessage;

class ContactsController extends Controller
{
    public function get(Request $request)
    {
        // get all users except the authenticated one
        $contacts = User::where('id', '!=', $request->id_aut)->get();

        // get a collection of items where sender_id is the user who sent us a message
        // and messages_count is the number of unread messages we have from him
        $unreadIds = Message::select(\DB::raw('`from` as sender_id, count(`from`) as messages_count'))
            ->where('to', $request->id_aut)
            ->where('read', false)
            ->groupBy('from')
            ->get();

        // add an unread key to each contact with the count of unread messages
        $contacts = $contacts->map(function($contact) use ($unreadIds) {
            $contactUnread = $unreadIds->where('sender_id', $contact->id)->first();

            $contact->unread = $contactUnread ? $contactUnread->messages_count : 0;

            return $contact;
        });


        //return response()->json($contacts);
        return response()->json(['status_code' => 201, 'resource' => $contacts]);
    }

    public function getMessagesFor(Request $request)
    {
        $id = (int)$request->id_conversacion;
        $autenticado = (int)$request->id_aut;
        auth()->id() == $autenticado;
        Message::where('from', $id)->where('to', $autenticado)->update(['read' => true]);

        // get all messages between the authenticated user and the selected user
        $messages = Message::where(function($q) use ($id,$autenticado) {
            $q->where('from', $autenticado);
            $q->where('to', $id);
        })->orWhere(function($q) use ($id,$autenticado) {
            $q->where('from', $id);
            $q->where('to', $autenticado);
        })
        ->get();


    return response()->json(['status_code' => 201, 'resource' => $messages/*[0]->text*/]);
        //return response()->json($messages);
    }

    public function send(Request $request)
    {
        $autenticado = (int)$request->id_aut;
        $id =(int)$request->contact_id;
        $message = Message::create([
            'from' => $autenticado,
            'to' => $id,
            'text' => $request->text
        ]);

        broadcast(new NewMessage($message));

        //return response()->json($message);
    return response()->json(['status_code' => 201, 'resource' => $message/*[0]->text*/]);

    }
}
