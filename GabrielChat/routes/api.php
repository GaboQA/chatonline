<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::post('/prueba/b', [App\Http\Controllers\PruebaController::class, 'prueba']);


//Obtener datos de los usuarios
Route::post('/contacts', [App\Http\Controllers\ContactsController::class, 'get']);
//Enviar Id de usuario autenticado y id de usuario seleccionado
Route::post('/conversation/info', [App\Http\Controllers\ContactsController::class, 'getMessagesFor']);
//Enviar conversacion 
Route::post('/conversation/send', [App\Http\Controllers\ContactsController::class, 'send']);
